;; This is the only true way to use spaces instead of tabs, and indent
;; to 4 spaces
(defun karim/set-indent-4()
  (setq c-basic-offset 4)
  (setq-default tab-width 4))

(defun karim/ensure-directory (arg)
  (if (file-exists-p arg)
      (concat arg " exists")
    (make-directory arg t) (concat arg " created")))


(defun karim/remap-up-key-in-shell ()
  (local-set-key (kbd "<up>") 'comint-previous-input)
  (local-set-key (kbd "<down>") 'comint-next-input))

(defun karim/cur-line-up ()
  (interactive)
  (transpose-lines 1)
  (forward-line -2))

(defun karim/cur-line-down ()
  (interactive)
  (forward-line 1)
  (transpose-lines 1)
  (forward-line -1))

(defun xml-format ()
  (interactive)
  (shell-command-on-region 1 (point-max) "xmllint --format -" (current-buffer) t)
)

