(use-package json-reformat
	     :ensure t)
(use-package json-snatcher
	     :ensure t)
(use-package json-mode
	     :ensure t)

(add-hook 'json-mode-hook
		  (lambda ()
			(make-local-variable 'js-indent-level)
			(setq js-indent-level 2)
			(setq indent-tabs-mode nil)))
