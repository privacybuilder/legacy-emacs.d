(use-package elpy
  :ensure t
  :defer t
  :init
  (advice-add 'python-mode :before 'elpy-enable)

  :custom
  (elpy-rpc-backend "jedi")
  (elpy-rpc-timeout 5)

  :config
  ;; flycheck instead of flymake
  (when (load "flycheck" t t)
    (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
    (add-hook 'elpy-mode-hook 'flycheck-mode))

  ;; Use IPython for REPL
  (setq python-shell-interpreter "jupyter"
	python-shell-interpreter-args "console --simple-prompt"
	python-shell-prompt-detect-failure-warning nil)
  (add-to-list 'python-shell-completion-native-disabled-interpreters
	       "jupyter")

  ;; blacken our code
  (add-hook
   'elpy-mode-hook
   (lambda ()
	 (add-hook 'before-save-hook
			   'elpy-black-fix-code nil t)))

  (defalias 'workon 'pyvenv-workon))

(use-package ein
  :after
  elpy
  :config
  (setq ein:completion-backend 'ein:use-ac-jedi-backend))
