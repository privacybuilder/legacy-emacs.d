;; ledger-mode
(use-package ledger-mode
  :defer t
  :config
  (add-to-list 'auto-mode-alist '("\\.lgr$" . ledger-mode))
  (add-to-list 'auto-mode-alist '("\\.txs$" . ledger-mode))
  (add-to-list 'auto-mode-alist '("\\.ledger$" . ledger-mode))
  (setq ledger-binary-path "/usr/local/bin/ledger"))
