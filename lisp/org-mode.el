;; my org setup

;; general settings
(setq org-directory "~/Documents/org")
(when (eq system-type 'darwin)
  (setq org-directory "~/Dropbox/Documents/org"))

;; org-agenda-files is set in custom
;;(setq org-agenda-files (quote ("~/Documents/org")))
;;(when (eq system-type 'darwin)
;;  (setq org-agenda-files (quote ("~/Dropbox/Documents/org"))))

(setq org-agenda-sticky t)
(setq org-catch-invisible-edits 'show)
(setq org-completion-use-ido t)

;; tree visibility niceties
(setq org-startup-indented t)

(with-eval-after-load 'org       
  (add-hook 'org-mode-hook #'visual-line-mode))

;; blank lines are evil.
; hide them between headings
(setq org-cycle-separator-lines 6)

; prevent creating blank lines before headings but allows list items to adapt to existing blank lines around the items
(setq org-blank-before-new-entry (quote ((heading)
                                         (plain-list-item . auto))))

(setq org-insert-heading-respect-content nil)

;; show notes at the top of the headline
(setq org-reverse-note-order nil)

;; allow hierarchical lists to have special behavior in agendas
(setq org-agenda-dim-blocked-tasks t)

;; search niceties
(setq org-show-following-heading t)
(setq org-show-hierarchy-above t)
(setq org-show-siblings (quote ((default))))

;; default table export is csv
(setq org-table-export-default-format "orgtbl-to-csv")

;; languages I org
(setq org-babel-load-languages '((emacs-lisp . t) (ledger . t) (dot . t) (sql . t)))

;; github flavored markdown
(eval-after-load "org"
  '(require 'ox-gfm nil t))

;; when changing list indent, go back to '-' lists
(setq org-list-demote-modify-bullet (quote (("+" . "-")
                                            ("*" . "-")
                                            ("1." . "-")
                                            ("1)" . "-")
                                            ("A)" . "-")
                                            ("B)" . "-")
                                            ("a)" . "-")
                                            ("b)" . "-")
                                            ("A." . "-")
                                            ("B." . "-")
                                            ("a." . "-")
                                            ("b." . "-"))))


;; for '<* TAB' code blocks, emit them lowercase
(setq org-structure-template-alist
  '(("a" . "export ascii")
    ("c" . "center")
    ("C" . "comment")
    ("e" . "example")
    ("E" . "export")
    ("h" . "export html")
    ("l" . "export latex")
    ("q" . "quote")
    ("s" . "src")
    ("v" . "verse")))

;; allow alphabetical lists
(setq org-alphabetical-lists t)

;; flyspell mode for spell checking everywhere
;(add-hook 'org-mode-hook 'turn-on-flyspell 'append)


;; make sure org knows we utf-8 all the things
(setq org-export-coding-system 'utf-8)

;; make all of the matched headings for a tag show at the same level in the agenda
(setq org-tags-match-list-sublevels t)

;; Disable keys in org-mode
;;    C-c [  agenda file manip
;;    C-c ]
;;    C-c ;
;;    C-c C-x C-q  cancelling the clock (we never want this)
(add-hook 'org-mode-hook
          '(lambda ()
             ;; Undefine C-c [ and C-c ] since this breaks my
             ;; org-agenda files when directories are include It
             ;; expands the files in the directories individually
             (org-defkey org-mode-map "\C-c[" 'undefined)
             (org-defkey org-mode-map "\C-c]" 'undefined)
             (org-defkey org-mode-map "\C-c;" 'undefined)
             (org-defkey org-mode-map "\C-c\C-x\C-q" 'undefined))
          'append)

; Use the current window for indirect buffer display
(setq org-indirect-buffer-display 'current-window)

;; make key org functionality available globally
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cc" 'org-capture)
(global-set-key "\C-cb" 'org-iswitchb)

;; fontify code in code blocks
(setq org-src-fontify-natively t)

;; make ctrl-a org-aware
(setq org-special-ctrl-a/e t)

;; function to make M-m behave the way I mean. This is for when muscle memory doesn't hit C-a
(defun org-back-to-indentation ()
  "Move point to the first non-whitespace & non-headline asterisk character on this line."
  (interactive "^")
  (beginning-of-line 1)
  (skip-syntax-forward " _" (line-end-position))
  ;; Move back over chars that have whitespace syntax but have the p flag.
  (backward-prefix-chars))

;; set bindings only pertinent to org-mode
(defun set-org-mode-bindings ()
;	    (local-set-key (kbd "ESC-M-<RET>") 'org-insert-heading-respect-content)
	    (local-set-key (kbd "M-m") 'org-back-to-indentation))
(add-hook 'org-mode-hook 'set-org-mode-bindings)

;; GTD
(setq karim-gtd-tasks (concat org-directory "/tasks.org"))
(setq karim-gtd-projects (concat org-directory "/projects.org"))
(setq karim-gtd-someday (concat org-directory "/someday.org"))
(setq karim-videate (concat org-directory "/videate-notes.org"))

;; capture templates
(setq org-capture-templates '(("i" "Inbox Item" entry
                               (file+headline karim-gtd-tasks "Inbox")
                               "* %i%?")
			      ("t" "TODO Item" entry
                               (file+headline karim-gtd-tasks "Inbox")
                               "* TODO %i%?")
			      ("v" "Videate Inbox" entry
                               (file+headline karim-videate "Inbox")
                               "* TODO %i%?")
			      ))

;; refile config
(setq org-refile-targets `((,karim-gtd-projects :maxlevel . 2)
			   (,karim-gtd-someday :level . 1)
			   (,karim-gtd-tasks :maxlevel . 1)
			   (,karim-videate :maxlevel . 2)
			   ))

;; TODO keywords
(setq org-todo-keywords '((sequence "TODO(t)" "IN-PROGRESS(i)" "PAUSED(p)" "WAITING(w)" "FINALIZE(f)" "|" "DONE(d)" "MISSED(m)" "CANCELLED(c)")
						  (sequence "NOTE(n)" "|" "ARCHIVED(a)" )))
(setq org-log-done 'time) ;; when moving to DONE create an entry
(setq org-todo-keyword-faces
      '(("TODO" :foreground "firebrick3" :weight bold)
		("PAUSED" :foreground "firebrick3" :weight bold)
		("WAITING" :foreground "yellow")
		("IN-PROGRESS" :foreground "green" :weight bold)
		("NOTE" :foreground "orange" :weight bold)
		("FINALIZE" :foreground "yellow" :weight bold)))

;; global tags
;;(setq org-tag-alist '((:startgroup . nil)
;; 		      ("@work" . ?w) ("@home" . ?h)
;; 		      (:endgroup . nil)
;; 		      (:startgroup . nil)
;; 		      ("laptop" . ?l) ("phone" . ?p)
;; 		      (:endgroup . nil)
;; 		      (:startgroup . nil)
;; 		      ("errands" . ?e) ("groceries" . ?g) ("costco" . ?c)
;; 		      (:endgroup . nil)))
;;;(setq org-tag-list nil)

;; archive 
(setq org-archive-location (concat org-directory "/archive/%s::datetree/**** Archived"))

;; Timestamp niceties, well they would be if setting this didn't break editing dates...
;(setq-default org-display-custom-times nil)
;;(setq org-time-stamp-custom-formats '("<%m/%d/%y %a>" . "<%m/%d/%y %a %H:%M>")) ; original 
;(setq org-time-stamp-custom-formats '("<%b %e, %Y>" . "<%b %e, %Y %H:%M>")) ; mine

;; auto-save org files after some time so that I can switch between
;; devices without dataloss/conflicts
(add-hook 'auto-save-hook 'org-save-all-org-buffers)

;; exporters, currently broken
;(require 'ox-md)
;(require 'ox-odt)


;; Some people view a TODO item that has been scheduled for execution or have a deadline (see Timestamps) as no longer open. Configure the variables:
;;   - org-agenda-todo-ignore-scheduled to exclude some or all scheduled items from the global TODO list,
;;   - org-agenda-todo-ignore-deadlines to exclude some or all items with a deadline set,
;;   - org-agenda-todo-ignore-timestamp to exclude some or all items with an active timestamp other than a DEADLINE or a SCHEDULED timestamp and/or
;;   - org-agenda-todo-ignore-with-date to exclude items with at least one active timestamp. 
