;; Mac spell checker is aspell
(setq ispell-program-name "/usr/local/bin/aspell"
      mac-command-modifier 'meta
      frame-title-format '("%b")
      ring-bell-function 'ignore)

(setq mac-allow-anti-aliasing t)

;; Work around a bug on OS X where system-name is FQDN
(setq system-name (car (split-string system-name "\\.")))

(global-set-key (kbd "M-`") 'next-multiframe-window)

(setq ls-lisp-use-insert-directory-program nil)
(require 'ls-lisp)
