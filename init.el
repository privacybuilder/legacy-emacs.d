;;; init.el
;;
;; "Emacs outshines all other editing software in approximately the
;; same way that the noonday sun does the stars. It is not just bigger
;; and brighter; it simply makes everything else vanish."
;;
;;          - Neal Stephenson, "In the Beginning was the Command Line"
;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Warm up

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

;; sometimes melpa sigs get out of sync. I often don't care
;;(setq package-check-signature nil)

;; location for locally installed lisp (not via package)
(setq local-lisp-directory (concat user-emacs-directory "local/"))

;; location for user (me!) created lisp
(setq user-lisp-directory (concat user-emacs-directory "lisp/"))

;; location for state files
(setq user-state-directory (concat user-emacs-directory "transient/"))

;; location for package files
(setq package-user-dir (concat user-emacs-directory "packages/"))

;; helper functions
(load (concat user-lisp-directory "functions.el"))

;; common lisp bindings
(require 'cl)

;; ensure PATH is set to find local installs
(setenv "PATH" (concat "/usr/local/bin:" (getenv "PATH")))

;; Don't clutter home dir with state files - each mode can use this variable
(karim/ensure-directory user-state-directory)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; fundamentals

;; Network Security Manager (NSM) files go in state dir
(setq nsm-settings-file (concat user-state-directory "network-security.data"))

;; Turn off mouse interface early in startup to avoid momentary display. ew. mice.
(if (fboundp 'menu-bar-mode) (menu-bar-mode -1))
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))

;; non-terminal setup
(when window-system
  (setq frame-title-format '(buffer-file-name "%f" ("%b")))
  (tooltip-mode -1)
  (mouse-wheel-mode t)
  (blink-cursor-mode -1)

  ;; 3 lines to get exec-path from shell
  ;(add-to-list 'load-path (concat package-user-dir "exec-path-from-shell-1.12"))
  ;(require 'exec-path-from-shell)
					;(exec-path-from-shell-initialize)
  )


;; Turn off UI elements, default to org-mode for new buffers
(setq inhibit-startup-message t
	  inhibit-splash-screen t
      initial-scratch-message nil
      initial-major-mode 'org-mode)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; package wants this to be first since "This must come before
;; configurations of installed packages."
(require 'package)

;; set archives
(setq package-archives
      '(
		("gnu" . "http://elpa.gnu.org/packages/")
		("melpa" . "https://melpa.org/packages/")
        ;("melpa-stable" .  "https://stable.melpa.org/packages/")
		)
	  )
(package-initialize)
(require 'use-package)

;; make sure all the used packages are installed
(require 'use-package-ensure)
(setq use-package-always-ensure t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Backups
;;
;; Don't clutter
(setq backup-directory-alist `((".*" . ,(concat user-state-directory "backups/"))))
(setq delete-old-versions -1)
(setq version-control t)
(setq vc-make-backup-files t)
(setq backup-by-copying t)
(setq auto-save-directory (expand-file-name (concat user-state-directory "auto-save-list/")))
(karim/ensure-directory auto-save-directory)
(setq auto-save-list-file-prefix auto-save-directory)
(setq tramp-auto-save-directory auto-save-directory)
(setq tramp-persistency-file-name (concat user-state-directory "tramp-persistency"))
(setq auto-save-file-name-transforms `((".*" ,auto-save-directory t)))
(setq tramp-persistency-file-name (concat user-state-directory "tramp"))
(setq create-lockfiles nil)

;; make emacs use the clipboard
(setq x-select-enable-clipboard t)

;; Transparently open compressed files
(auto-compression-mode t)

;; Enable syntax highlighting for older Emacsen that have it off
(global-font-lock-mode t)

;; Highlight matching parentheses when the point is on them.
(show-paren-mode 1)

;; ffap: aka most annoying feature ever. This flag enforces C-u prefix
;; so that I may never ever feel its affects. For the love of all that
;; is holy, even the docs know it sucks: M-x describe-variable
;; ffap-require-prefix says "Experts may prefer to disable ffap most
;; of the time." Consider me an expert.
(setq ffap-require-prefix t)

;; almost always coding, almost always want column numbers...
(setq column-number-mode t)

;; Font size
(define-key global-map (kbd "C-+") 'text-scale-increase)
(define-key global-map (kbd "C--") 'text-scale-decrease)

;; Use regex searches by default.
(global-set-key (kbd "C-s") 'isearch-forward-regexp)
(global-set-key (kbd "\C-r") 'isearch-backward-regexp)
(global-set-key (kbd "C-M-s") 'isearch-forward)
(global-set-key (kbd "C-M-r") 'isearch-backward)

;; move through windows with the keyboard
(global-set-key (kbd "C-c <C-left>") 'windmove-left)
(global-set-key (kbd "C-c <C-down>") 'windmove-down)
(global-set-key (kbd "C-c <C-up>") 'windmove-up)
(global-set-key (kbd "C-c <C-right>") 'windmove-right)

;; M-R does some window movement stuff by default. Want
;; ripgrep. Requires installation of ripgrep on underlying OS.
(use-package use-package-ensure-system-package)
(use-package rg
  :config
  (setq rg-enable-menu t)
  :ensure-system-package
  (rg . ripgrep))
(global-set-key (kbd "M-r") 'rg)


;; these are disabled by cur-line-up/down
;(define-key global-map [(meta up)] '(lambda() (interactive) (scroll-other-window -1)))
;(define-key global-map [(meta down)] '(lambda() (interactive) (scroll-other-window 1)))

;; I am all grown up now, y/n is as good as yes/no
(defalias 'yes-or-no-p 'y-or-n-p)

;; donno why this is disabled by default...
(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)

;; use ctrl-x,c,v for cut,copy,paste, ctrl-z for undo
(cua-mode t)

;; allow lines to be moved up and down like org-mode does it
(global-set-key (kbd "M-<up>") 'karim/cur-line-up)
(global-set-key (kbd "M-<down>") 'karim/cur-line-down)

;; ibuffer > buffer manager
(global-set-key (kbd "C-x C-b") 'ibuffer)

;; ivy > helm > ido
(use-package ivy
  :config
  (setq ivy-use-virtual-buffers t)
  (setq enable-recursive-minibuffers t)
  (setq ivy-count-format "(%d/%d) ")
  (setq ivy-use-selectable-prompt t)
  (setq ivy-magic-tilde nil))

(ivy-define-key ivy-minibuffer-map (kbd "TAB") #'ivy-partial)

(use-package hydra)
(use-package ivy-hydra)

;; utf-8 all the things
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(prefer-coding-system 'utf-8)
(set-charset-priority 'unicode)
(setq default-process-coding-system '(utf-8-unix . utf-8-unix))


;; vestigial parts from esk, someday figure out if I really want these
(setq fringe-mode (cons 4 0)
      echo-keystrokes 0.1
      font-lock-maximum-decoration t
      transient-mark-mode t
      color-theme-is-global t
      mouse-yank-at-point t
      truncate-partial-width-windows nil
      uniquify-buffer-name-style 'forward
      whitespace-style '(trailing lines space-before-tab
                                  face indentation space-after-tab)
      whitespace-line-column 100
      ediff-window-setup-function 'ediff-setup-windows-plain
      xterm-mouse-mode t
      save-place-file (concat user-state-directory "places"))

;; Push-Button Compile
(global-set-key (kbd "<f9>") 'compile)

;; sql niceties
(add-to-list 'auto-mode-alist '("\\.sql$" . sql-mode))
(add-to-list 'auto-mode-alist '("\\.hql$" . sql-mode))
(use-package sql-indent)
(eval-after-load "sql"
  '(load-library "sql-indent"))

;; misc language configs
(add-to-list 'auto-mode-alist '("\\.jsp$" . java-mode))

;; Default to unified diffs
(setq diff-switches "-u -w")

;; remap up key in shell mode
(add-hook 'shell-mode-hook 'karim/remap-up-key-in-shell)

;; colorize all the things in shell mode
(add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on)

;; custom settings live where I can check them into vcs
(setq custom-file (concat user-lisp-directory "custom.el"))
(load custom-file)

;; make emacs not split horizontally
;(setq split-height-threshold 1200)
;(setq split-width-threshold 2000)

;; dired extra
(add-hook 'dired-load-hook
	  (lambda ()
	    (load "dired-x")
	    ;; Set dired-x global variables here.  For example:
	    ;; (setq dired-guess-shell-gnutar "gtar")
	    ;; (setq dired-x-hands-off-my-keys nil)
	    (setq dired-omit-files (concat dired-omit-files "\\|^\\.DS_Store$"))
	    ))
(add-hook 'dired-mode-hook
	  (lambda ()
	    ;; Set dired-x buffer-local variables here.  For example:
	    ;; (dired-omit-mode 1)
	    ))

;; packages we like, nearly every file we open
(require 'uniquify)
(require 'saveplace)
(require 'url-util) ;; url mojo

;; xml mode indent
(setq nxml-child-indent 4 nxml-attribute-indent 4)

;; yaml mode
(use-package yaml-mode)
(add-to-list 'auto-mode-alist '("\\.ya?ml$" . yaml-mode))


;; Mac setup
(when (eq system-type 'darwin)
  (load (concat user-lisp-directory "osx.el")))

;; subtle bell
(setq ring-bell-function
      (lambda ()
        (let ((orig-fg (face-foreground 'mode-line)))
          (set-face-foreground 'mode-line "red")
          (run-with-idle-timer 0.1 nil
                               (lambda (fg) (set-face-foreground 'mode-line fg))
                               orig-fg))))


(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

(use-package diminish)

;; complete anything - company :D
(use-package company
  :bind (:map company-active-map
			  ("<esc>" . company-abort)
			  ("<tab>" . company-complete-selection)
			  ("C-n" . company-select-next)
			  ("C-p" . company-select-previous))
  :config
  (setq company-idle-delay 0)
  (setq global-company-mode t)
  (setq company-minimum-prefix-length 1))

;; Give Emacs Psychic Completion Powers
;; https://github.com/daviwil/emacs-from-scratch/blob/master/show-notes/Emacs-Tips-Prescient.org
(use-package counsel)
(use-package prescient
  :config
  ;(setq prescient-sort-length-enable nil)
  (prescient-persist-mode 1))
(use-package ivy-prescient
  :after counsel
  :config
  (ivy-prescient-mode 1))
(use-package company-prescient
  :after company
  :config
  (company-prescient-mode 1))

(use-package which-key)
(which-key-mode)

;; magit requires bookmark, and we want to customize so require it here
(use-package bookmark
  :config
  (setq bookmark-default-file (concat user-state-directory "bookmarks")))

(use-package magit
  :custom
  (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1)
  :config
  (setq magit-highlight-whitespace nil)
  (setq magit-highlight-trailing-whitespace nil)
  (setq magit-highlight-indentation nil)
  (setq magit-diff-refine-hunk nil)
  (global-set-key (kbd "C-x g") 'magit-status))

(setq auth-sources '("~/.authinfo"))


;; NOTE VERY IMPORTANT
;;
;; This setup requires you install flycheck-32. The way I did that was
;; to copy latests from mainline in github to the flycheck-31 package
;; dir, delete the elc's, then restart emacs.
(use-package flycheck)

(use-package go-mode)

;; Install prettier-js if not already installed
(use-package prettier-js
  :ensure t)

(defun karim/js-setup ()
  "Common setup for TypeScript, JavaScript, and JSX."
  (lsp-deferred)
  ; tab width for all
  (add-hook 'js2-mode-hook (lambda () (setq tab-width 2)))
  (add-hook 'typescript-mode-hook (lambda () (setq tab-width 2)))
  (add-hook 'web-mode-hook (lambda () (setq tab-width 2)))
  ; prettier for all
  (add-hook 'js2-mode-hook 'prettier-js-mode)
  (add-hook 'web-mode-hook 'prettier-js-mode)
  (add-hook 'typescript-mode-hook 'prettier-js-mode)
  (add-hook 'before-save-hook 'prettier-js nil t)
  ; web mode customizations
  (add-hook 'web-mode-hook (lambda () (setq web-mode-enable-auto-quoting nil))))

;; js ecosystem modes
(dolist (mode-hook '(typescript-mode-hook js2-mode-hook js2-jsx-mode-hook web-mode-hook))
  (add-hook mode-hook 'karim/js-setup))

;;; TypeScript mode for .ts and .tsx
;(use-package typescript-mode
;  :ensure t
;  :mode ("\\.ts\\'" "\\.tsx\\'"))

;; web-mode for .ts and .tsx
(use-package web-mode
  :ensure t
  :mode ("\\.ts\\'" "\\.tsx\\'"))

;; JavaScript and JSX modes
(use-package js2-mode
  :ensure t
  :mode ("\\.js\\'" "\\.cjs\\'" "\\.jsx\\'")
  :interpreter "node")

;; if you install a new lsp-mode, make sure you add the necessry bin paths to exec-path in lisp/custom.el
(use-package lsp-mode
  :diminish
  :commands (lsp lsp-deferred)
  :init
  (setq lsp-keymap-prefix "C-c l")
  (setq lsp-ui-doc-show-with-cursor nil)
  (setenv "GOPATH" (concat (getenv "HOME") "/go"))
  (setenv "PATH" (concat (getenv "PATH") ":" (concat (getenv "GOPATH") "/bin")))
  (setenv "PATH" (concat (getenv "PATH") ":/usr/local/go/bin"))
  (setenv "PATH" (concat (getenv "PATH") ":" (concat (getenv "HOME") "/.nvm/active-version")))

  :config
  (add-to-list 'lsp-enabled-clients 'ts-ls)
  (add-to-list 'lsp-enabled-clients 'eslint)
  (lsp-enable-which-key-integration t)
  (lsp-headerline-breadcrumb-mode)
  (karim/set-indent-4))

(use-package lsp-ui :commands lsp-ui-mode)
(use-package dap-mode)


;; Set up before-save hooks to format buffer and add/delete imports.
(defun karim/lsp-go-install-save-hook ()
  (add-hook 'before-save-hook #'lsp-format-buffer t t)
  (add-hook 'before-save-hook #'lsp-organize-imports t t))
(add-hook 'go-mode-hook #'karim/lsp-go-install-save-hook)

;; Start LSP Mode and YASnippet mode
(add-hook 'go-mode-hook #'lsp-deferred)
(add-hook 'go-mode-hook #'yas-minor-mode)

(defun karim/switch-project-action ()
  "Switch to a workspace with the project name and start `magit-status'."
  (persp-switch (projectile-project-name))
  (magit-status))

(use-package projectile
  :diminish projectile-mode
  :config (projectile-mode)
  :bind-keymap
  ("C-c p" . projectile-command-map)
  :init
  (when (file-directory-p "~/src")
	(setq projectile-project-search-path '("~/src")))
  (setq projectile-switch-project-action #'karim/switch-project-action))

(use-package counsel-projectile
  :config (counsel-projectile-mode))

(use-package dockerfile-mode)

(use-package simple-httpd
  :init
  (setq httpd-port 12080))

(use-package string-inflection
  :config
  (global-set-key (kbd "C-c C-u") 'string-inflection-all-cycle)
)

;; moar init
(load (concat user-lisp-directory "org-mode.el"))
(load (concat user-lisp-directory "org-roam.el"))
(load (concat user-lisp-directory "elpy.el"))
(load (concat user-lisp-directory "graphviz.el"))
(load (concat user-lisp-directory "json.el"))
(load (concat user-lisp-directory "css.el"))
;(load (concat user-lisp-directory "tide.el"))
(load (concat user-lisp-directory "ledger.el"))
;(load (concat user-lisp-directory "ruby.el"))

(add-hook 'terraform-mode-hook
          (lambda ()
            (add-hook 'before-save-hook 'delete-trailing-whitespace nil t)))

(add-hook 'yaml-mode-hook
          (lambda ()
            (add-hook 'before-save-hook 'delete-trailing-whitespace nil t)))
